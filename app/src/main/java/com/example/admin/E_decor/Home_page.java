package com.example.admin.E_decor;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.facebook.AccessToken;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import static com.google.android.gms.common.internal.safeparcel.SafeParcelable.NULL;

public class Home_page extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, TabLayout.OnTabSelectedListener {

    private TabLayout tabLayout;
    TextView t1;
    private  ImageLoader imageLoader;
    private ViewPager viewPager;
    private GoogleApiClient mgoogleapiclient;
    private GoogleSignInApi ln;
    CircularNetworkImageView profile_img;
    public SharedPreferences pr;
    public SharedPreferences.Editor editor;
    public Boolean save;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText("Home"));
        tabLayout.addTab(tabLayout.newTab().setText("Categories"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.pager);

        //Creating our pager adapter
        final Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount());

        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener((TabLayout.OnTabSelectedListener) this);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        t1=(TextView)header.findViewById(R.id.Profile_name);
        profile_img=(CircularNetworkImageView)header.findViewById(R.id.profile_image);
        pr=getSharedPreferences("my pref",MODE_PRIVATE);
        String s=pr.getString("Name","hi user");
        String p=pr.getString("pic",NULL);
        t1.setText(s);


       imageLoader = CustomVolleyRequest.getInstance(this.getApplicationContext())
                .getImageLoader();

        imageLoader.get(p,
                ImageLoader.getImageListener(profile_img,
                        R.mipmap.ic_launcher,
                        R.mipmap.ic_launcher));

        //Loading image
        profile_img.setImageUrl(p, imageLoader);

    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            return true;
        }
       else if (id == R.id.location) {

            Intent i=new Intent(getApplicationContext(),Show_city_name.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {


        }
        else if (id == R.id.nav_decoration) {

            Intent i=new Intent(getApplicationContext(),Decoration.class);
            startActivity(i);
        }
        else if (id == R.id.nav_catering) {

            Intent i=new Intent(getApplicationContext(),Catering.class);
            startActivity(i);

        }
        else if (id == R.id.nav_event) {

            Intent i=new Intent(getApplicationContext(),Event.class);
            startActivity(i);

        }
        else if (id == R.id.nav_Offerzone) {

            Intent i=new Intent(getApplicationContext(),Offerzone.class);
            startActivity(i);

        }
        else if (id == R.id.nav_myaccount) {

            Intent i=new Intent(getApplicationContext(),My_account.class);
            startActivity(i);

        }
        else if (id == R.id.nav_myorder) {

            Intent i=new Intent(getApplicationContext(),My_order.class);
            startActivity(i);

        }
        else if (id == R.id.nav_cart) {

            Intent i=new Intent(getApplicationContext(),My_cart.class);
            startActivity(i);

        }

        else if (id == R.id.customer_support) {

            Intent i=new Intent(getApplicationContext(),Customer_support.class);
            startActivity(i);
        }
        else if (id == R.id.nav_logout) {

            signout();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void signout() {

        Auth.GoogleSignInApi.signOut(mgoogleapiclient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {

                pr=getSharedPreferences("my pref",MODE_PRIVATE);
                editor=pr.edit();
                editor.clear();
                editor.commit();
                Intent i=new Intent(getApplicationContext(),Login.class);
                startActivity(i);

            }
        });
    }
    protected void onStart()
    {
        GoogleSignInOptions gso=new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mgoogleapiclient=new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
        mgoogleapiclient.connect();
        super.onStart();
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab)
    {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
