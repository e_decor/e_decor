package com.example.admin.E_decor;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;



public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

    ArrayList<ListProvider>arrayList=new ArrayList<>();
    Context ctx;
    public RecyclerAdapter(ArrayList<ListProvider>arrayList,Context ctx)
    {
        this.arrayList=arrayList;
        this.ctx=ctx;
    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view;
        RecyclerViewHolder recyclerViewHolder;
        view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_layout,parent,false);
        recyclerViewHolder =new RecyclerViewHolder(view,ctx,arrayList);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        ListProvider listProvider;
        listProvider =arrayList.get(position);
        holder.list_img.setImageResource(listProvider.getImg_id());
        holder.list_name.setText(listProvider.getName());

        Glide.with(ctx).load(R.drawable.wedding_decoration)
                .thumbnail(0.5f)
                .crossFade()
                .into(holder.list_img);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {

        ImageView list_img;
        TextView list_name;
        Context ctx;
        ArrayList<ListProvider> arrayList = new ArrayList<ListProvider>();


        public RecyclerViewHolder(View view, Context ctx, ArrayList<ListProvider> arrayList) {
            super(view);
            view.setOnClickListener(this);
            this.arrayList = arrayList;
            this.ctx = ctx;
            view.setOnClickListener(this);
            list_img = (ImageView) view.findViewById(R.id.decimg);
            list_name = (TextView) view.findViewById(R.id.decname);
            view.setOnClickListener(this);
            //Glide.with(ctx).load()

        }
        
        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
            ListProvider listprovider = this.arrayList.get(position);
            Intent intent= new Intent(this.ctx,Ganesh_Dec.class);
            this.ctx.startActivity(intent);

        }
    }
}
