package com.example.admin.E_decor;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.identity.intents.Address;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class Location extends FragmentActivity implements OnMapReadyCallback {


    private static final String TAG = Location.class.getSimpleName();
    private static final int READ_CONTACTS_PERMISSIONS_REQUEST = 1;
     private Context mContext ;
    private GoogleMap mMap;
    FloatingActionButton loc;
    TextView t1;
    TextView city_name,area_name;




    @NeedsPermission({android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION })
    void showLocation() {
        // location permission
    }

    @OnShowRationale({android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION})
    void showRationaleForLocation(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage("Location permission needed to get Location")
                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        request.proceed();
                    }
                })
                .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        request.cancel();
                    }
                })
                .show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        LocationPermissionsDispatcher.showLocationWithCheck(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        city_name=(TextView)findViewById(R.id.city_name);








            }


    private void getcityname(double lat, double lon) {

        String city="";
        String area="";
        Geocoder geocoder = new Geocoder(Location.this, Locale.getDefault());
        List<android.location.Address> addresses;
        try {
            Log.i("log_tag", "latitude" + lat);
            Log.i("log_tag", "longitude" + lon);
            addresses = geocoder.getFromLocation(lat, lon, 1);
            Log.v("log_tag", "addresses+)_+++" + addresses);
            city = addresses.get(0).getLocality();
            area = addresses.get(0).getSubLocality();
            Log.v("log_tag","AreaName :"+area);
            Log.v("log_tag", "CityName" +":" +city);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        city_name.setText(city+","+area);

        // \n is for new line
        Toast.makeText(
                getApplicationContext(),
                "Your Location is - \nLat: " + lat
                        + "\nLong: " + lon, Toast.LENGTH_LONG)
                .show();
        SharedPreferences sharedPreferences=getSharedPreferences("my pre",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("City",city);
        editor.commit();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        LocationPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera

        LatLng sydney = new LatLng(23.0137782, 72.5047225);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
