package com.example.admin.E_decor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class My_cart extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("My cart");
    }
}
