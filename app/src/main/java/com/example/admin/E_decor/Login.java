package com.example.admin.E_decor;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static com.example.admin.E_decor.R.layout.login;

public class Login extends AppCompatActivity implements View.OnClickListener ,GoogleApiClient.OnConnectionFailedListener{

    Button b1,g_login,fb_login;
    EditText e1,e2;
    TextView t1;
    private  GoogleApiClient mgoogleapiclient;
    private CallbackManager callbackManager;
    private GoogleSignInOptions gso;
    private int RC_SIGN_IN = 100;
    public Boolean save;
    public SharedPreferences pr;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.login);
        setTitle("Login");
        setTheme(R.style.alerdialog);
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        if (ni != null && ni.isConnected())
        {
            Toast.makeText(getApplicationContext(),"internet is on",Toast.LENGTH_LONG).show();
        }

        else {

            new AlertDialog.Builder(this)
                    .setTitle("Info")
                    .setMessage("Internet Connection not available")
                    .setPositiveButton("Open Setting", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent i=new Intent(Settings.ACTION_SETTINGS);
                            startActivity(i);
                        }
                    })
                    .setNegativeButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.cancel();
                        }
                    })
                    .show();
        }
        LocationManager l1=(LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (l1.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {

        }
        else
        {
            new AlertDialog.Builder(this)
                    .setTitle("Location")
                    .setMessage("Location is turnOff")
                    .setPositiveButton("Open setting", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setNegativeButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.cancel();
                        }
                    });
        }
        g_login=(Button)findViewById(R.id.G_login);
        fb_login=(Button)findViewById(R.id.fb_login);
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                Toast.makeText(getApplicationContext(),"sucesss",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {

                Toast.makeText(getApplicationContext(),"Cancel",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException e) {

                Toast.makeText(getApplicationContext(),"Error ",Toast.LENGTH_LONG).show();
            }
        });
        fb_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LoginManager.getInstance().logInWithReadPermissions(Login.this, Arrays.asList("public_profile", "user_friends"));
            }
        });



        e1 = (EditText) findViewById(R.id.Email_id);
        t1 = (TextView)findViewById(R.id.register_account) ;
        e2 = (EditText) findViewById(R.id.password);
        b1 = (Button) findViewById(R.id.Signin_button);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = e1.getText().toString();
                String passwd = e2.getText().toString();

                if (!isValidEmail(email)) {
                    e1.setError("Invalid Email");
                }
                if (!isValidPass(passwd)) {
                    e2.setError("Invalid Password");
                } else if (isValidEmail(email) && isValidPass(passwd)) {

                    Intent i=new Intent(Login.this,Home_page.class);
                    startActivity(i);

                }
            }

        });
        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(getApplicationContext(),Signup.class);
                startActivity(i);
            }
        });

        //initialized Google sign in option
        gso=new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        //initialized google api client
        mgoogleapiclient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();

        g_login.setOnClickListener(this);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If signin
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);
        }
        if(callbackManager.onActivityResult(resultCode,requestCode,data))
        {
            return;
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();
            pr=getSharedPreferences("my pref",MODE_PRIVATE);
            SharedPreferences.Editor editor=pr.edit();
            editor.putString("Name",acct.getDisplayName());
            editor.putString("pic",acct.getPhotoUrl().toString());
            editor.commit();
            editor.clear();
            Intent i=new Intent(getApplicationContext(),Home_page.class);
            startActivity(i);
            //Displaying name and email


        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern p1 = Pattern.compile(EMAIL_PATTERN);
        Matcher m1 = p1.matcher(email);
        return m1.matches();
    }

    private boolean isValidPass(String passwd) {
        return passwd != null && passwd.length() > 6;
    }


    @Override
    public void onClick(View v) {
        if (v == g_login) {
            //Calling signin
            signIn();
        }

    }

    private void signIn() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mgoogleapiclient);
        startActivityForResult(signInIntent , RC_SIGN_IN);
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }



}
