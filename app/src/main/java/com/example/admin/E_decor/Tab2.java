package com.example.admin.E_decor;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


//Our class extending fragment
public class Tab2 extends Fragment  {


    CardView dec,cat,photo_grapher,event;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

View view = inflater.inflate(R.layout.tab2, container, false);

        dec=(CardView)view.findViewById(R.id.dec);
        cat=(CardView)view.findViewById(R.id.cate);
        photo_grapher=(CardView)view.findViewById(R.id.photo);
        event=(CardView)view.findViewById(R.id.event);
        dec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(getActivity(),Decoration.class);
                startActivity(i);
            }
        });
        cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(getActivity(),Catering.class);
                startActivity(i);

            }
        });
        photo_grapher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(getActivity(),Catering.class);
                startActivity(i);

            }
        });
        event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(getActivity(),Event.class);
                startActivity(i);

            }
        });
        return view;


    }


}
