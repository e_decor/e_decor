package com.example.admin.E_decor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class  Signup extends AppCompatActivity {


    EditText username,email_id,password,mobile_no;
    Button signup_btn;
    public static final String ROOT_URL = "http://backendedecor.16mb.com/";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Signup");
        username=(EditText)findViewById(R.id.Username);
        email_id=(EditText)findViewById(R.id.Email_id_signup);
        password=(EditText)findViewById(R.id.Password);
        mobile_no=(EditText)findViewById(R.id.Mobile_no);
        signup_btn=(Button)findViewById(R.id.Signup_button);
        signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                insertUser();
            }
        });

    }

    private void insertUser() {

        RestAdapter adapter=new RestAdapter.Builder()
                        .setEndpoint(ROOT_URL)
                        .build();

        RegisterApi api=adapter.create(RegisterApi.class);
        api.insertUser(

                username.getText().toString(),
                email_id.getText().toString(),
                password.getText().toString(),
                mobile_no.getText().toString(),

                new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {

                        BufferedReader reader=null;
                        String output="";
                        try
                        {
                            reader=new BufferedReader(new InputStreamReader(response.getBody().in()));
                            output=reader.readLine();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(Signup.this, output, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Toast.makeText(Signup.this, error.toString(),Toast.LENGTH_LONG).show();
                    }
                }

        );

    }
}
