package com.example.admin.E_decor;

import retrofit.Callback;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;


public interface RegisterApi {
    @FormUrlEncoded
    @POST("/retofit/insert.php")
    public void insertUser(
            @Field("username") String username,
            @Field("email_id") String email_id,
            @Field("password") String password,
            @Field("mobile_no") String mobile_no,
            Callback<Response> callback);
}