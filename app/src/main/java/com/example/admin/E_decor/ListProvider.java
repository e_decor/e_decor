package com.example.admin.E_decor;



public class ListProvider {

    private int img_id;
    private String name;

    public ListProvider(int img_id,String name)
    {
        this.setImg_id(img_id);
        this.setName(name);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImg_id() {
        return img_id;
    }

    public void setImg_id(int img_id) {
        this.img_id = img_id;
    }
}
