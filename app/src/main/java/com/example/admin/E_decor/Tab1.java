package com.example.admin.E_decor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


//Our class extending fragment
public class Tab1 extends Fragment {



    private LinearLayout linearLayout;
    private int dot_count;
    Context ctx;
    private ImageView[] dots;
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ViewPager viewPager;



    private static final Integer[] IMAGES={R.drawable.album2,R.drawable.album5,R.drawable.catering};

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.tab1, container, false);


        viewPager = (ViewPager)view.findViewById(R.id.slider);
        ViewPagerAdapter madapter=new ViewPagerAdapter(getActivity(),ImagesArray);
        viewPager.setAdapter(madapter);
        // Auto start of viewpager
        for(int i=0;i<IMAGES.length;i++)
            ImagesArray.add(IMAGES[i]);
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator


        return view;
    }

}

