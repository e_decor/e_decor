package com.example.admin.E_decor;

import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class Show_city_name extends AppCompatActivity {


    private static final String TAG = Show_city_name.class.getSimpleName();
    TextView c1;
    String s="";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_city_name);
        c1=(TextView)findViewById(R.id.city_name);



        Mylocation.LocationResult locationResult = new Mylocation.LocationResult() {
            @Override
            public void gotLocation(android.location.Location location) {
                double lat = location.getLatitude();
                double lon = location.getLongitude();
                Log.e(TAG, "Location: " + lat + ", " + lon);
                getcityname(lat,lon);



            }
        };
        Mylocation myLocation = new Mylocation();
        myLocation.getLocation(Show_city_name.this, locationResult);

    }
    private void getcityname(double lat, double lon) {

        String city="";
        String area="";
        Geocoder geocoder = new Geocoder(Show_city_name.this, Locale.getDefault());
        List<Address> addresses;
        try {
            Log.i("log_tag", "latitude" + lat);
            Log.i("log_tag", "longitude" + lon);
            addresses = geocoder.getFromLocation(lat, lon, 1);
            Log.v("log_tag", "addresses+)_+++" + addresses);
            city = addresses.get(0).getLocality();
            area = addresses.get(0).getSubLocality();
            Log.v("log_tag","AreaName :"+area);
            Log.v("log_tag", "CityName" +":" +city);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        c1.setText(area+" , "+city);


        // \n is for new line
        Toast.makeText(
                getApplicationContext(),
                "Your Location is - \nLat: " + lat
                        + "\nLong: " + lon, Toast.LENGTH_LONG)
                .show();


    }
}
